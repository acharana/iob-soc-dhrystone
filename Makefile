SOC_DIR = submodules/iob-soc

#commit of HEAD in iob-soc git submodule
SOC_COMMIT = $(shell cd $(SOC_DIR);git rev-parse HEAD)

sim:
	cp rtl/include/system.vh        $(SOC_DIR)/rtl/include
	cp rtl/src/system.v             $(SOC_DIR)/rtl/src
	cp software/firmware/Makefile   $(SOC_DIR)/software/firmware
	cp software/firmware/dhry*      $(SOC_DIR)/software/firmware
	cp software/firmware/syscalls.c $(SOC_DIR)/software/firmware
	cp software/firmware/stdlib.c   $(SOC_DIR)/software/firmware
	make -C $(SOC_DIR) sim

test:        copy_files          run_sims   run_sims_O3
test_la:     copy_files_la       run_sims   run_sims_O3
test_ddr:    copy_files_ddr      run_sims   run_sims_O3
test_ddr_la: copy_files_ddr_la   run_sims   run_sims_O3

copy_files:
	$(eval RESULTS_DIR=results/normal)
	@echo ""
	@echo "START DHRYSTONE TEST OF IOBSOC"
	@echo ""
	cp rtl/include/system_normal.vh  $(SOC_DIR)/rtl/include/system.vh
	cp rtl/src/system.v              $(SOC_DIR)/rtl/src
	cp software/firmware/dhry*       $(SOC_DIR)/software/firmware
	cp software/firmware/syscalls.c  $(SOC_DIR)/software/firmware
	cp software/firmware/stdlib.c    $(SOC_DIR)/software/firmware

copy_files_la:
	$(eval RESULTS_DIR=results/la)
	@echo ""
	@echo "START DHRYSTONE TEST OF IOBSOC WITH LA"
	@echo ""
	cp rtl/include/system_la.vh      $(SOC_DIR)/rtl/include/system.vh
	cp rtl/src/system.v              $(SOC_DIR)/rtl/src
	cp software/firmware/dhry*       $(SOC_DIR)/software/firmware
	cp software/firmware/syscalls.c  $(SOC_DIR)/software/firmware
	cp software/firmware/stdlib.c    $(SOC_DIR)/software/firmware

copy_files_ddr:
	$(eval RESULTS_DIR=results/ddr)
	@echo ""
	@echo "START DHRYSTONE TEST OF IOBSOC WITH DDR"
	@echo ""
	cp rtl/include/system_ddr.vh     $(SOC_DIR)/rtl/include/system.vh
	cp rtl/src/system.v              $(SOC_DIR)/rtl/src
	cp software/firmware/dhry*       $(SOC_DIR)/software/firmware
	cp software/firmware/syscalls.c  $(SOC_DIR)/software/firmware
	cp software/firmware/stdlib.c    $(SOC_DIR)/software/firmware

copy_files_ddr_la:
	$(eval RESULTS_DIR=results/ddr_la)
	@echo ""
	@echo "START DHRYSTONE TEST OF IOBSOC WITH DDR AND LA"
	@echo ""
	cp rtl/include/system_ddr_la.vh  $(SOC_DIR)/rtl/include/system.vh
	cp rtl/src/system.v              $(SOC_DIR)/rtl/src
	cp software/firmware/dhry*       $(SOC_DIR)/software/firmware
	cp software/firmware/syscalls.c  $(SOC_DIR)/software/firmware
	cp software/firmware/stdlib.c    $(SOC_DIR)/software/firmware

run_sims:
	@echo ""
	@echo "SIMULATION WITH 100 RUNS"
	@echo ""
	cp software/firmware/Makefile_100runs $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs100.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 100 RUNS"
	@echo ""
	@echo ""
	@echo "SIMULATION WITH 500 RUNS"
	@echo ""
	cp software/firmware/Makefile_500runs $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs500.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 500 RUNS"
	@echo ""
	@echo ""
	@echo "SIMULATION WITH 1000 RUNS"
	@echo ""
	cp software/firmware/Makefile_1000runs $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs1000.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 1000 RUNS"
	@echo ""

run_sims_O3:
	@echo ""
	@echo "SIMULATION WITH 100 RUNS WITH THE -O3 FLAG"
	@echo ""
	cp software/firmware/Makefile_100runs_O3 $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs100_O3.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 100 RUNS WITH THE -O3 FLAG"
	@echo ""
	@echo ""
	@echo "SIMULATION WITH 500 RUNS WITH THE -O3 FLAG"
	@echo ""
	cp software/firmware/Makefile_500runs_O3 $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs500_O3.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 500 RUNS WITH THE -O3 FLAG"
	@echo ""
	@echo ""
	@echo "SIMULATION WITH 1000 RUNS WITH THE -O3 FLAG"
	@echo ""
	cp software/firmware/Makefile_1000runs_O3 $(SOC_DIR)/software/firmware/Makefile
	make -C $(SOC_DIR) sim > $(RESULTS_DIR)/runs1000_O3.txt
	make -C $(SOC_DIR) clean
	@echo ""
	@echo "ENDED SIMULATION WITH 1000 RUNS WITH THE -O3 FLAG"
	@echo ""

clean: 
	rm -f *~
	rm -f rtl/include/*~
	rm -f software/firmware/*~
	rm -f simulation/*~
	make -C $(SOC_DIR) clean
	rm -f $(SOC_DIR)/software/firmware/dhry*
	rm -f $(SOC_DIR)/software/firmware/syscalls.c
	rm -f $(SOC_DIR)/software/firmware/stdlib.c
	rm -f $(SOC_DIR)/software/firmware/Makefile_*

veryclean: clean
	cd $(SOC_DIR); git reset --hard $(SOC_COMMIT)

.PHONY: sim clean veryclean test_all test test_la test_ddr test_ddr_la copy_files copy_files_la copy_files_ddr copy_files_ddr_la run_sims run_sims_O3
