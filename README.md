# IOb-SoC-Dhrystone

The Dhrystone benchmark running on IOb-SoC.

At the time of writing, the only tested RTL simulator is Icarus Verilog.

* IObSoC repository:
``https://bitbucket.org/jjts/iob-soc``
* Dhrystone benchmark code for a PicoRV32 CPU (used by IObSoC):
``https://github.com/cliffordwolf/picorv32/tree/master/dhrystone``




## Update IObSoC's Git submodule:

```bash
git submodule update --init --recursive
```



## Edit the system configuration file: ``rtl/include/system.vh``

Comment or uncomment the following macros:

* ``USE_DDR``;
* ``USE_LA_IF``;

Leave ``USE_BOOT`` uncommented, because it does not change Dhrystone's results and it increases
simulation time.




## Edit the system source file: ``rtl/src/system.v``
Specify the following IOb-Cache parameters (or not, if you wish to use their default values):

* ``NLINE_W``;
* ``OFFSET_W``;
* ``NWAY_W``;




## Edit the firmware Makefile: ``software/firmware/Makefile``

* Edit the ``NUMBER_OF_RUNS`` environment variable;
* Add/remove the ``-O3`` flag to/from RISC-V GCC;




## Run an RTL simulation:

```bash
make sim
```

This copies the system's source and configuration files, the firmware files and the firmware
Makefile to IObSoC's submodule directory tree and runs an RTL simulation using IObSoC's submodule
Makefile tree.




## Run a test:

```bash
make test
make test_la
make test_ddr
make test_ddr_la
```

A test is a set of 6 different simulations for one of the 4 possible system configurations. The 6 simulations are:

* 100 runs;
* 500 runs;
* 1000 runs;
* 100 runs with the ``-O3`` flag;
* 500 runs with the ``-O3`` flag;
* 1000 runs with the ``-O3`` flag;

Remember to edit the Cache's parameters in ``rtl/src/system.v`` before running a test that uses the DDR.

Dhrystone results are stored in ``.txt`` files in:

* ``results/normal`` when running ``make test``;
* ``results/la`` when running ``make test_la``;
* ``results/ddr`` when running ``make test_ddr``;
* ``results/ddr_la`` when running ``make test_ddr_la``;




## Clean the repo:

* Use ``make clean`` to clean the repo and the IObSoC submodule.

* Use ``make veryclean`` to clean the repo and also ``git reset --hard`` IObSoC's submodule to the commit pointed by its ``HEAD``.